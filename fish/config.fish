#if status is-interactive
    # Commands to run in interactive sessions can go here
    #end
    # .config/fish/config.fish or .config/fish/fish_profile

# Check if the terminal is tty1 and start X if it is
#if test (tty) = "/dev/tty1"
#   startx
#end


#####################################
set -U fish_greeting

starship init fish | source

#alias ls 'exa'

#alias ll 'ls -l'

#alias ll 'exa --group-directories-first -lah'
#######Scripts###########
#neofetch

colorscript random
#./fm6000 --random --color random
###############################################################

# .config/fish/config.fish

# If not running interactively, don't do anything
if status is-interactive
    #    alias ls 'ls --color=auto'
    #  set -g -x fish_prompt '[\u@\h \W]\$ '

    # xbps
    alias i 'doas xbps-install -S'
    function u
        i
        doas xbps-install -u xbps
        doas xbps-install -u
    end
    alias q 'doas xbps-query -Rs'
    alias r 'doas xbps-remove -R'
    #    colorscript random

    # starship
    #
    #    status --is-command-substitution; and starship init fish | source

    alias ls 'exa'

    # ./fm6000 --random --color random
    alias ll 'exa --group-directories-first -lah'
    alias at 'alacritty-themes'

    set -U fish_key_bindings fish_vi_key_bindings
end


### AUTOCOMPLETE AND HIGHLIGHT COLORS ###
set fish_color_normal brcyan
set fish_color_autosuggestion '#7d7d7d'
set fish_color_command brcyan
set fish_color_error '#ff6c6b'
set fish_color_param brcyan

# Start dbus session if not already started

