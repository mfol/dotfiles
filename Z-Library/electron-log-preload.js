
      try {
        (function o({contextBridge:u,ipcRenderer:d}){if(!d)return;d.on("__ELECTRON_LOG_IPC__",(h,g)=>{window.postMessage({cmd:"message",...g})}),d.invoke("__ELECTRON_LOG__",{cmd:"getOptions"}).catch(h=>console.error(new Error(`electron-log isn't initialized in the main process. Please call log.initialize() before. ${h.message}`)));const v={sendToMain(h){try{d.send("__ELECTRON_LOG__",h)}catch(g){console.error("electronLog.sendToMain ",g,"data:",h),d.send("__ELECTRON_LOG__",{cmd:"errorHandler",error:{message:g==null?void 0:g.message,stack:g==null?void 0:g.stack},errorName:"sendToMain"})}},log(...h){v.sendToMain({data:h,level:"info"})}};for(const h of["error","warn","info","verbose","debug","silly"])v[h]=(...g)=>v.sendToMain({data:g,level:h});if(u&&process.contextIsolated)try{u.exposeInMainWorld("__electronLog",v)}catch{}typeof window=="object"?window.__electronLog=v:__electronLog=v})(require('electron'));
      } catch(e) {
        console.error(e);
      }
    