/*
 * Outputs "Hello, World!" and then exits
 */

public class HelloWorld {
   public static void main(String[] args) {
       System.out.println("Hello, World!");
   }
}
