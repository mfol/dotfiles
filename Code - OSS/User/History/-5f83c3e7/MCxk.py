#! /usr/bin/python3

def remove_exclamation_marks(s):
    name=""
    for i in range(len(s)):
        if "!" in s[i]:
            continue
        name+=s[i]
    return name
##########################################

print(remove_exclamation_marks("!!!A!l!i!"))
